<?php

namespace Drupal\twig_image;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\File\FileUrlGenerator;
use Drupal\Core\Image\ImageFactory;
use Drupal\Core\Image\ImageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\file\FileInterface;
use Drupal\file\Plugin\Field\FieldType\FileFieldItemList;
use Drupal\image\ImageStyleInterface;
use Drupal\image\Plugin\Field\FieldType\ImageItem;
use Drupal\media\Entity\Media;
use Drupal\media\MediaInterface;

/**
 * The ImageManager service.
 */
class ImageManager {
  /**
   * The configuration manager.
   *
   * @var \Drupal\Core\Config\ConfigManagerInterface
   */
  protected $configManager;

  /**
   * The entity_type.repository service.
   *
   * @var \Drupal\Core\Entity\EntityTypeRepositoryInterface
   */
  protected $entityTypeManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The plugin.manager.image.effect service.
   *
   * @var \Drupal\Core\Cache\CacheableDependencyInterface
   */
  protected $imageEffect;

  /**
   * ImageFactory.
   *
   * @var \Drupal\Core\Image\ImageFactory
   */
  protected $imageFactory;

  /**
   * Is CropModule enabled.
   *
   * @var bool
   */
  protected $cropModuleEnabled;

  /**
   * ImageFile.
   *
   * @var mixed
   */
  protected $imageFile;

  /**
   * Is WebpModule enabled.
   *
   * @var bool
   */
  protected $isWebpModuleEnabled;

  /**
   * The webp.
   *
   * @var mixed
   */
  protected $webp;

  /**
   * LanguageManager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * FileUrlGenerator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * Constructs a new ImageManager object.
   *
   * @param \Drupal\Core\Config\ConfigManagerInterface $configManager
   *   The configuration manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity_type.repository service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\Core\Cache\CacheableDependencyInterface $imageEffect
   *   The plugin.manager.image.effect service.
   * @param \Drupal\Core\Image\ImageFactory $imageFactory
   *   The imageFactory.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   Logger Factory.
   * @param Drupal\Core\File\FileUrlGenerator $fileUrlGenerator
   *   FileUrlGenerator.
   */
  public function __construct(
        ConfigManagerInterface $configManager,
        EntityTypeManagerInterface $entityTypeManager,
        ModuleHandlerInterface $moduleHandler,
        CacheableDependencyInterface $imageEffect,
        ImageFactory $imageFactory,
        LanguageManagerInterface $language_manager,
        FileUrlGenerator $fileUrlGenerator
    ) {
    $this->configManager = $configManager;
    $this->entityTypeManager = $entityTypeManager;
    $this->moduleHandler = $moduleHandler;
    $this->imageEffect = $imageEffect;
    $this->imageFactory = $imageFactory;
    $this->cropModuleEnabled = $this->moduleHandler->moduleExists('crop');
    $this->isWebpModuleEnabled = $this->moduleHandler->moduleExists('webp');
    $this->languageManager = $language_manager;
    $this->fileUrlGenerator = $fileUrlGenerator;
    if ($this->isWebpModuleEnabled) {
      $this->webp = \Drupal::service('webp.webp');
    }
  }

  /**
   * Get twig image data for render array twig-image.html.twig template.
   *
   * @param mixed $data
   *   To get image from.
   *
   * @return mixed
   *   TwigImage in data.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getTwigImage($data) {
    $data = $this->normalizeData($data);

    $images = [];
    $imagesFromField = $this->getImageItemsFromInput($data['image']);

    foreach ($imagesFromField as $indexImage => $image) {
      $this->setImageFile($image);

      $imageFile = $this->getImageFile();
      if (!($imageFile instanceof ImageInterface)) {
        // We couldn't get a valid image.
        continue;
      }

      if (!$imageFile->isValid()) {
        // If stage file proxy exists, try to render image anyway.
        if (!$this->moduleHandler->moduleExists('stage_file_proxy')) {
          continue;
        }
      }

      foreach ($data['sources'] as $source) {
        if ($this->isWebpModuleEnabled) {
          $images[$indexImage]['sources'][] = $this->getImageStyleSrcForSource($source, TRUE);
        }
        $images[$indexImage]['sources'][] = $this->getImageStyleSrcForSource($source);
      }

      // Get fallback image.
      if (!empty($data['image'][$indexImage]['#image_style'])) {
        $fallBackImageStyle = $this->getImageStyleByName($data['image'][$indexImage]['#image_style']);
        $images[$indexImage]['fallback'] = $fallBackImageStyle->buildUrl($imageFile->getSource());
      }
      else {
        $images[$indexImage]['fallback'] = $this->fileUrlGenerator->generateAbsoluteString(
              $imageFile->getSource()
          );
      }

      // Set alt text if available.
      if (isset($image["alt"])) {
        $images[$indexImage]['alt'] = $image["alt"];
      }
    }

    $data['images'] = $images;

    return $data;
  }

  /**
   * Sets the image file.
   *
   * @param mixed $image
   *   The image array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function setImageFile(
        $image
    ) {
    $propertyType = (isset($image['target_id'])) ? 'fid' : 'uri';
    $property = $image['target_id'] ?? $image['#uri'];
    $file = $this->entityTypeManager->getStorage('file')->loadByProperties([$propertyType => $property]);
    $file = is_array($file) ? reset($file) : NULL;

    if ($file instanceof FileInterface) {
      $imageFile = $this->imageFactory->get($file->getFileUri());

      if ($imageFile instanceof ImageInterface) {
        $this->imageFile = $imageFile;
      }
    }
  }

  /**
   * Getter for ImageFile.
   *
   * @return \Drupal\Core\Image\ImageInterface
   *   ImageFile returns.
   */
  protected function getImageFile() {
    return $this->imageFile;
  }

  /**
   * Return image style.
   *
   * @param array $dimensions
   *   Array with dimensions for image.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   Returns the style for the image.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getImageStyle(
        array $dimensions = [
          'width'  => '',
          'height' => '',
          'crop'   => '',
        ]
    ) {
    $imageStyleName = $this->getTwigImageStyleName($dimensions);
    $imageStyle = $this->getImageStyleByName($imageStyleName);
    $imageStyle = $this->validateImageStyle($imageStyle, $dimensions);

    if (empty($imageStyle)) {
      $imageStyle = $this->createImageStyle($dimensions, $imageStyleName);
    }

    return $imageStyle;
  }

  /**
   * Get image_style by name.
   *
   * @param mixed $imageStyleName
   *   Name of image style to get.
   *
   * @return mixed
   *   Image style.
   */
  protected function getImageStyleByName($imageStyleName) {
    return $this->entityTypeManager->getStorage('image_style')->load($imageStyleName);
  }

  /**
   * Validates the image style.
   *
   * @param mixed $imageStyle
   *   Style of the image.
   * @param mixed $dimensions
   *   Dimensions to use.
   * @param bool $recreated
   *   Is it recreated.
   *
   * @return \Drupal\image\ImageStyleInterface|mixed
   *   Returns mixed
   */
  protected function validateImageStyle(
        $imageStyle,
        $dimensions,
        $recreated = FALSE
    ) {
    if ($imageStyle instanceof ImageStyleInterface && !empty($dimensions["crop"])) {
      $containsCrop = FALSE;
      foreach ($imageStyle->getEffects() as $effect) {
        if ($effect->getPluginId() === 'crop_crop') {
          $containsCrop = TRUE;
          break;
        }
      }

      if (!$containsCrop) {
        $fullImageStyleName = $imageStyle->id();

        // Try to recreate the image style once.
        if (!$recreated) {
          $imageStyle->delete();
          $newImageStyle = $this->createImageStyle($dimensions, $fullImageStyleName);

          // See if the new one is valid.
          return $this->validateImageStyle($newImageStyle, $dimensions, TRUE);
        }
        else {
          \Drupal::logger('twig_image')->error("Unable to create image style $fullImageStyleName with cropping functionality.");
        }
      }
    }

    return $imageStyle;
  }

  /**
   * Get the name of the TwigImageStyle.
   *
   * @param array $dimensions
   *   Dimensions array to use.
   *
   * @return string
   *   TwigImageStyle name
   */
  protected function getTwigImageStyleName(array $dimensions) {
    $imageStyleNameStructure = ['twig_image'];

    if ($this->cropModuleEnabled && $cropDimension = $dimensions['crop'] ?? NULL) {
      $imageStyleNameStructure[] = $cropDimension;
    }

    if ($widthDimension = $dimensions['width'] ?? NULL) {
      $imageStyleNameStructure[] = $widthDimension;
    }

    if ($heightDimension = $dimensions['height'] ?? NULL) {
      $imageStyleNameStructure[] = $heightDimension;
    }

    return implode('_', $imageStyleNameStructure);
  }

  /**
   * Create twig_image styles.
   *
   * Try and cacth to prevent errors for multiple same requests.
   *
   * @param array $dimensions
   *   Dimensions used.
   * @param string $imageStyleName
   *   Name of the image style.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   Return created image style or null.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function createImageStyle(array $dimensions, $imageStyleName) {
    try {
      $imageStyle = $this->entityTypeManager->getStorage('image_style')->create(
            [
              'name'  => $imageStyleName,
              'label' => $imageStyleName,
            ]
        );

      // Add crop image effect if needed.
      if ($this->cropModuleEnabled) {
        if (!empty($dimensions['crop'])) {
          // Get the crop type.
          $cropTypeIds = $this->entityTypeManager->getStorage('crop_type')->getQuery()
            ->condition('id', $dimensions['crop'])
            ->execute();

          if (empty($cropTypeIds)) {
            $cropType = NULL;
          }
          else {
            $cropType = $this->entityTypeManager->getStorage('crop_type')->load(current($cropTypeIds));
          }

          // If the crop type exists, add as image effect.
          if ($cropType !== NULL) {
            $configuration = [
              'uuid'   => NULL,
              'id'     => 'crop_crop',
              'weight' => 1,
              'data'   => [
                'crop_type' => $dimensions['crop'],
              ],
            ];
            $effectCrop = $this->imageEffect->createInstance($configuration['id'], $configuration);
            $imageStyle->addImageEffect($effectCrop->getConfiguration());
          }
        }
      }

      // Add image scale effect if either width or height were supplied.
      if (!empty($dimensions['width']) || !empty($dimensions['height'])) {
        // Prepare image effect scale configuration.
        $configuration = [
          'uuid'   => NULL,
          'weight' => 1,
          'id'     => (empty($dimensions['width']) || empty($dimensions['height'])) ? 'image_scale' : 'image_scale_and_crop',
          'data'   => [
            'upscale' => FALSE,
            'width'   => (!empty($dimensions['width']) && $dimensions['width'] > 0) ? $dimensions['width'] : NULL,
            'height'  => (!empty($dimensions['height']) && $dimensions['height'] > 0) ? $dimensions['height'] : NULL,
          ],
        ];

        $effectScale = $this->imageEffect->createInstance($configuration['id'], $configuration);
        $imageStyle->addImageEffect($effectScale->getConfiguration());
      }

      $imageStyle->save();
    }
    catch (EntityStorageException $e) {
      $imageStyle = $this->entityTypeManager->getStorage('image_style')->load($imageStyleName);
    }
    catch (Exception $e) {
      return NULL;
    }

    return $imageStyle;
  }

  /**
   * Normalize the data in for it to be used.
   *
   * @param mixed $data
   *   Not normalized data.
   *
   * @return mixed
   *   Normalized data.
   */
  protected function normalizeData($data) {
    // We get this if we pass something like 'node.field_image'
    // to the 'image' key.
    if ($data['image'] instanceof EntityReferenceFieldItemListInterface) {
      $imageEntity = $data['image'];
      $data['image'] = ['#items' => $imageEntity];
    }

    // We get this if we pass something like 'node.field_image.0'
    // to the 'image' key.
    if ($data['image'] instanceof EntityReferenceItem) {
      $maybeMediaEntityTarget = $data['image']->getValue();
      if ($targetId = $maybeMediaEntityTarget['target_id'] ?? NULL) {
        if (($mediaEntity = $this->entityTypeManager->getStorage('media')->load($targetId)) && $mediaEntity instanceof MediaInterface) {
          $data['image'] = ['#items' => $mediaEntity];
        }
      }
    }

    return $data;
  }

  /**
   * Get image items from input.
   *
   * @param mixed $image
   *   Image containing the items.
   *
   * @return array
   *   ImageItems array.
   */
  protected function getImageItemsFromInput($image) {
    // Check if input is a field.
    if ((!empty($image['#items']) || isset($image['#items'])) && is_object($image['#items'])) {
      return $this->getImageItemsFromImageField($image);
    }
    elseif (is_array($image) && isset($image['#uri'])) {
      return [$image];
    }
    elseif (is_array($image) && isset($image['target_id']) && isset($image['alt'])) {
      return [$image];
    }
    elseif (is_array($image) && isset($image['#item']) && $image['#item'] instanceof ImageItem) {
      $imageItem = $image['#item'];

      return [$imageItem->getValue()];
    }
    elseif (is_array($image) && isset($image['#media'])) {
      return $this->getImageItemsFromImageField($image);
    }
    else {
      return [];
    }
  }

  /**
   * Get possible cardinality image items from image field.
   *
   * @param mixed $imageField
   *   ImageField to use.
   *
   * @return array
   *   Possible cardinality image items from image field.
   */
  protected function getImageItemsFromImageField($imageField) {
    $images = [];

    if (!empty($imageField['#media'])) {
      return $this->guessImageFieldFromMedia($imageField['#media']);
    }

    if (empty($imageField['#items']) || !is_object($imageField['#items'])) {
      return $images;
    }

    if ($imageField['#items'] instanceof FileFieldItemList) {
      foreach ($imageField['#items']->getValue() as $image) {
        $images[] = $image;
      }
    }
    elseif ($imageField['#items'] instanceof EntityReferenceFieldItemListInterface) {
      $referencedEntities = $imageField['#items']->referencedEntities();
      foreach ($referencedEntities as $referencedEntity) {
        if ($referencedEntity instanceof Media) {
          // Try and guess the image field.
          $images = $this->guessImageFieldFromMedia($referencedEntity);
        }
      }
    }

    if ($imageField['#items'] instanceof MediaInterface) {
      $images = $this->guessImageFieldFromMedia($imageField['#items']);
    }

    return $images;
  }

  /**
   * Guess ImageField from media.
   *
   * @param mixed $mediaEntity
   *   MediaEntity used.
   *
   * @return array
   *   Data array.
   */
  protected function guessImageFieldFromMedia($mediaEntity) {
    $images = [];

    foreach (['field_image', 'field_media_image'] as $guess) {
      if ($mediaEntity->hasField($guess)) {
        if ($this->languageManager->isMultilingual()) {
          $currentLanguage = $this->languageManager->getCurrentLanguage()->getId();
          if ($mediaEntity->hasTranslation($currentLanguage)) {
            $mediaEntity = $mediaEntity->getTranslation($currentLanguage);
          }
        }

        foreach ($mediaEntity->get($guess) as $image) {
          // The rest of the module expects a simple data
          // array, not the image object.
          $images[] = $image->getValue();
        }
      }
    }

    return $images;
  }

  /**
   * Add image style source with available multipliers to the sources.
   *
   * @param mixed $source
   *   Image style source.
   * @param bool $webp
   *   Use webp.
   *
   * @return mixed
   *   Source.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getImageStyleSrcForSource($source, $webp = FALSE) {
    $initialWidth = NULL;
    $initialHeight = NULL;

    if (!empty($source['width'])) {
      // Round the value up.
      $source['width'] = ceil($source['width']);
      // Save original width value.
      $initialWidth = $source['width'];
    }

    if (!empty($source['height'])) {
      // Round the value up.
      $source['height'] = ceil($source['height']);
      // Save original height value.
      $initialHeight = $source['height'];
    }

    // Generate 1x version.
    $imageStyle = $this->getImageStyle($source);
    $srcset = $imageStyle->buildUrl($this->getImageFile()->getSource()) . ' 1x';

    // Check for multipliers.
    if (!empty($source['multipliers']) && is_array($source['multipliers'])) {
      foreach ($source['multipliers'] as $multiplier) {
        // Duplicate $source to not edit the original attributes.
        $calculatedSource = $source;

        if (!empty($multiplier) && $multiplier !== 1) {
          if (!empty($initialWidth)) {
            // Calculate the multiplied image size.
            $calculatedSource['width'] = $initialWidth * $multiplier;
          }

          if (!empty($initialHeight)) {
            // Calculate the multiplied image size.
            $calculatedSource['height'] = $initialHeight * $multiplier;
          }

          $imageStyle = $this->getImageStyle($calculatedSource);
          $url = $imageStyle->buildUrl($this->getImageFile()->getSource());

          $srcset .= ', ' . $url . ' ' . $multiplier . 'x';
        }
      }
    }

    if ($webp) {
      $source['srcset'] = $this->webp->getWebpSrcset($srcset);
      $source['type'] = 'image/webp';
    }
    else {
      $source['srcset'] = $srcset;
      $source['type'] = 'image/jpeg';
    }

    return $source;
  }

}
