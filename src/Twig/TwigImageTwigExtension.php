<?php

namespace Drupal\twig_image\Twig;

use Drupal\twig_image\ImageManager;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Twig extension.
 */
class TwigImageTwigExtension extends AbstractExtension {


  /**
   * The image manager.
   *
   * @var \Drupal\twig_image\ImageManager
   */
  protected $imageManager;

  /**
   * Constructs a new TwigImageTwigExtension object.
   *
   * @param \Drupal\twig_image\ImageManager $imageManager
   *   used imageManager.
   */
  public function __construct(
        ImageManager $imageManager
    ) {
    $this->imageManager = $imageManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new TwigFunction(
              'twigImage', [
                $this,
                'twigImage',
              ]
      ),
    ];
  }

  /**
   * Creating the twigImage.
   *
   * @param array $data
   *   In array.
   *
   * @return array
   *   returns twigImage array
   */
  public function twigImage(array $data = []) {
    return [
      '#theme' => 'twig_image',
      '#data'  => $this->imageManager->getTwigImage($data),
    ];
  }

}
