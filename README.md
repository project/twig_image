# Twig image 8.x
---------------

## About this Module

With this module you will be enable to auto generate the correct image styles based upon a new twig function twigImage()


### Goals

- Auto generate responsive images with the correct image styles based upon twig defined specs, without the need to manual add all image styles for responsive images




## Installing 

1. Copy/upload the twig_image module to the modules directory of your Drupal
   installation.

2. Enable the 'Twig image' module and desired sub-modules in 'Extend'. 
   (/admin/modules)

3. Define a image field (with in the display the default image style for fallback)

4. You render a responsive image with the following example: 
```
{{ twigImage({
    image: content.field_image,
    sources: [
        {
            crop: 'portrait',
            width: 800,
            height: 400,
            media: '(min-width: 800em)',
            multipliers: [2]
        },
        {
            crop: 'landscape',
            width: 500,
            height: 400,
            media: '(min-width: 1200em)'
        }
    ],
    attributes: create_attribute().addClass('test').setAttribute('id', 'testid'),
    imgAttributes: create_attribute({
        'class': ['extraclass2'],
        id: 'uniqueId',
        loading: 'lazy',
        alt:'alt text override'
    }),
}) }}
```

## Recommendations
Use config_split and add "image.style.twig_image*" to the blacklist so they are ignored on importing.
